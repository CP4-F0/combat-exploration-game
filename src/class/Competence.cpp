
#include <iostream>
#include <fstream>

#include "Competence.h"

using namespace std;

/// Constructor.
Competence::Competence()
{
	OurPv=0;
	OurMana=0;
	TheirPv=0;
	TheirMana=0;
	description="Ne rien faire";
}

/// Overloaded constructor.
Competence::Competence(int ourPv, int ourMana, int theirPv, int theirMana, string descr)
{
	OurPv = ourPv;
	OurMana = ourMana;
	TheirPv = theirPv;
	TheirMana = theirMana;
	description = descr;
}

/// Sort of visual getter.
void Competence::display()
{
	cout << description <<" - Vous[" << OurPv << "PV |" << OurMana << " MANA] Lui[" << TheirPv << "PV |"<< TheirMana <<" MANA]." << endl;
}

/// String getter.
string Competence::getInfos(){
	return description +" - Vous["+ to_string(OurPv) +"PV |"+ to_string(OurMana) +"MANA] Lui["+ to_string(TheirPv) +"PV |"+ to_string(TheirMana) +"MANA].";
}

/// Getter.
int Competence::getOurPv(){
	return OurPv;
}

/// Getter.
int Competence::getOurMana(){
	return OurMana;
}

/// Getter.
int Competence::getTheirPv(){
	return TheirPv;
}

/// Getter.
int Competence::getTheirMana(){
	return TheirMana;
}

/// Getter.
string Competence::getDesc(){
	return description;
}
