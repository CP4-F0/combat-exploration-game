#include <iostream>
#include <cstdlib>
#include <ctime>
#include "Map.h"

using namespace std;

/// Map constructor, it generate a map with random structures on it.
Map::Map(){
	int size = 75; // we can modify this
	if(size > 250 || size < 10)
		return;

	this->sizeMap = size;
	cell = new Cell *[sizeMap];
	for(int i(0); i<sizeMap; i++){
		cell[i] = new Cell[sizeMap];
	}

	for(int i(0); i<sizeMap; i++){
		for(int j(0); j<sizeMap; j++){
			Cell c;
			cell[i][j] = c;
		}
	}

	srand(time(NULL));
	int centreX, centreY;

	int arbres = getRandomNumber(size/2)+getRandomNumber(size/2);
	for(int i(0); i<arbres; i++){
		centreX = getRandomNumber(size);
		centreY = getRandomNumber(size);
		Cell c(3);
		cell[centreX][centreY] = c;
	}

	for(int i(getRandomNumber(size/10)+1); i>0; i--){
		do{
			centreX = getRandomNumber(size);
			centreY = getRandomNumber(size);
		}while(centreX > size-3 || centreX < 3 || centreY > size-3 || centreY < 3);
		Cell c(3);
		for(int j(1); j<4; j++){
			cell[centreX+getRandomNumber(j)][centreY+getRandomNumber(j)] = c;
			cell[centreX-getRandomNumber(j)][centreY-getRandomNumber(j)] = c;
			cell[centreX+getRandomNumber(j)][centreY-getRandomNumber(j)] = c;
			cell[centreX-getRandomNumber(j)][centreY+getRandomNumber(j)] = c;
		}
	}

	int montagnes = getRandomNumber(size/5)+5;
	for(int i(0); i<montagnes; i++){
		do{
			centreX = getRandomNumber(size);
		}while(centreX > size-8 || centreX < 8);
		do{
			centreY = getRandomNumber(size);
		}while(centreY > size-8 || centreY < 8);
		
		// now we have a good position for a mountain
		Cell c(1);

		for(int j(1); j<5; j++){

			if(getRandomNumber(5)+1 == 3){
				if(centreX>centreY &&  centreX > 5){
					centreX-= 2;
				}
				else if(centreY > 5)
					centreY-= 2;
			}
			else if(getRandomNumber(5)+1 == 2){
				if(centreX>centreY &&  centreX < size-5){
					centreX+= 2;
				}
				else if(centreY < size-5)
					centreY+= 2;
			}

			for(int i(10); i>j; i--){
				cell[centreX+getRandomNumber(j)][centreY+getRandomNumber(j)] = c;
				cell[centreX-getRandomNumber(j)][centreY+getRandomNumber(j)] = c;
				cell[centreX+getRandomNumber(j)][centreY-getRandomNumber(j)] = c;
				cell[centreX-getRandomNumber(j)][centreY-getRandomNumber(j)] = c;
			}
		}

		Cell d(2);
		cell[centreX][centreY] = d;
		cell[centreX+getRandomNumber(2)][centreY+getRandomNumber(2)] = d;
		cell[centreX-getRandomNumber(2)][centreY+getRandomNumber(2)] = d;
		cell[centreX+getRandomNumber(2)][centreY-getRandomNumber(2)] = d;
		cell[centreX-getRandomNumber(2)][centreY-getRandomNumber(2)] = d;

	}

	int river = getRandomNumber(size/10)+3;
	for(int i(0); i<river; i++){
		do{
			centreX = getRandomNumber(size);
			centreY = getRandomNumber(size);
		}while(centreX > size-15 || centreX < 15 || centreY > size-15 || centreY < 15);
		
		// now we have a good position for a river

		Cell e(4);
		int riverLength = getRandomNumber(40)+getRandomNumber(30), direction = getRandomNumber(4)+1;
		for(int j(0); j<riverLength; j++){
			for(int k(getRandomNumber(3)+2); k>0; k--){
				if(direction == 1){
					if(j<riverLength && centreX > 2){
						cell[centreX][centreY] = e;
						centreX--;
						j++;
					}
				}
				else if(direction == 2){
					if(j<riverLength && centreY < size-2){
						cell[centreX][centreY] = e;
						centreY++;
						j++;
					}
				}
				else if(direction == 3){
					if(j<riverLength && centreX < size-2){
						cell[centreX][centreY] = e;
						centreX++;
						j++;
					}
				}
				else if(direction == 4){
					if(j<riverLength && centreY > 2){
						cell[centreX][centreY] = e;
						centreY--;
						j++;
					}
				}
			}
			direction = getRandomNumber(4)+1; // entre 1 et 4
		}
	}


	this->sizeMonsters = getRandomNumber(15)+getRandomNumber(15);

	int xMonster, yMonster;
	monster = new Entity[sizeMonsters];
	for(int i(0); i<sizeMonsters; i++){
		Entity e;
		
		do{
			xMonster = getRandomNumber(size);
			yMonster = getRandomNumber(size);
		}
		while(!canWeGoHere(xMonster, yMonster));
		// now we are sure we can walk to the monster

		e.modify("Dumb", getRandomNumber(3)+1, xMonster, yMonster, false);
		monster[i] = e;
	}

}

/// Destructor.
Map::~Map(){
	for(int i(sizeMap); i<sizeMap; i++)
		delete cell[i];
	delete[] cell;
}


/// The colored and beautiful screen is created here.
void Map::displayInterface(int x, int y){
	

	// BORDER TOP
	cout << "+";
	for(int i(0); i<20; i++)
		cout << "--";
	cout << "+" << endl;


	for(int i(x-10); i<x+10; i++){
		
		// BORDER LEFT
		cout << "|";

		for(int j(y-10); j<y+10; j++){
			// BACKGROUND
			if(i < 0 || j < 0 || i > sizeMap-1 || j > sizeMap-1)
				cout << notACellBackground << "  ";
			else{
				cout << cell[i][j].getCellBackgroundColor();

				// TEXT COLOR
				if(x == i && y ==j)
					cout << playerColor << "0" << basicTextColor << " ";
				else
					cout << cell[i][j].getCellAppearence() << " ";
			}
			cout << basicBackgroundColor;
		}

		//BORDER RIGHT
		cout << "|";
		cout << endl;
	}

	// BORDER BOOTOM
	cout << "+";
	for(int i(0); i<20; i++)
		cout << "--";
	cout << "+" << endl;

}

/// Sort of a getter. Debug feature.
void Map::listMonsters(){
	for(int i(0); i<sizeMap; i++)
		cout << "NAME[" <<  monster[i].getName() << "] PV[" << monster[i].getPv() << "] MANA[" << monster[i].getMana() << "] [" << monster[i].getPlayable() << "]" << endl;
}

/// Getter.
int Map::getNbMonsters(){
	int nbMonsters = 0;
	for(int i (0); i<sizeMonsters; i++)
		if(monster[i].isAlive())
			nbMonsters++;
	return nbMonsters;
}

/// Int getter (return the position of the monster by his localisation).
int Map::isThereAMonsterRightHere(int x, int y){
	for(int i(0); i<sizeMonsters; i++){
		if(monster[i].getX() == x && monster[i].getY() == y && monster[i].isAlive())
			return i;
	}
	return -1;
}

/// Instantly kill a monster. Debug feature.
bool Map::killMonster(int x, int y){
	for(int i(0); i<sizeMonsters; i++){
		if(monster[i].getX() == x && monster[i].getY() == y){
			monster[i].kill();
			return true;
		}
	}
	return false;	
}

/// Sort of setter.
bool Map::attack(int pv, int mana, int i){
	monster[i].setMana(monster[i].getMana()+mana);
	monster[i].setPv(monster[i].getPv()+pv);
	if(monster[i].getPv() <= 0){
		monster[i].kill();
		return true;
	}
	return false;
}

/// Getter.
int Map::getSizeMap(){
	return sizeMap;
}

/// Getter.
string Map::getAppearence(int x, int y){
	return cell[x][y].getCellDescription();
}

/// Getter.
int Map::getRandomNumber(int max){
	return (rand()%max);
}

/// Sort of visual getter.
string Map::displayInfosMonster(int idMonster){
	return monster[idMonster].getName() +" - PV["+ to_string(monster[idMonster].getPv()) +"/"+ to_string(monster[idMonster].getPvMax()) +"] MANA["+ to_string(monster[idMonster].getMana()) +"/"+ to_string(monster[idMonster].getManaMax()) +"] - "+ monster[idMonster].getSpeName();
}

/// Boolean getter.
bool Map::isMonsterDead(int i){
	return !monster[i].isAlive();
}

/// String getter.
string Map::getMonsterName(int i){
	return monster[i].getName();
}

/// Boolean getter.
bool Map::canWeGoHere(int x, int y){
	return cell[x][y].getCellStatus();
}

/// Useful for showing the position of a random monster.
string Map::getIndicationMonster(){
	// on cherche d'abord à prendre des monstres au pif
	for(int i(0); i<sizeMonsters; i++){
		int monsterRandom = getRandomNumber(sizeMonsters);
		if(!isMonsterDead(monsterRandom)){
			// encore un vestige de test unitaire
			//cout << "On a pris le monstre [" << monsterRandom << "/"<< sizeMonsters <<"]." << endl;
			return "["+ to_string(monster[monsterRandom].getX()-5+getRandomNumber(10)+1) +"]["+ to_string(monster[monsterRandom].getY()-5+getRandomNumber(10)+1) +"]";
		}
	}

	// si ça n'a pas marché alors on prend le premier monstre du tableau qu'on trouve vivant
	for(int i(0); i<sizeMonsters; i++){
		if(!isMonsterDead(i)){
			return "["+ to_string(monster[i].getX()-5+getRandomNumber(10)+1) +"]["+ to_string(monster[i].getY()-5+getRandomNumber(10)+1) +"]";
		}
	}
	return "[Erreur; vous êtes sensé avoir gagné]";
}

/// Getter.
int Map::getNbComp(int i){
	return monster[i].getNbComp(monster[i].getSpeInt());
}

/// Entyti getter.
Entity Map::getMonster(int i){
	return monster[i];
}