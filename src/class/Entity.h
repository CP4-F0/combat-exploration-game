#ifndef ENTITY_H
#define ENTITY_H

#include "Specialite.h"
#include "Competence.h"

class Entity
{
	private:
		std::string name;
		int pv;
		int pvMax;
		int mana;
		int manaMax;
		bool player;
		bool dead;
		int x;
		int y;
		Specialite spe;
		int nbComp;
		Competence * c;
	public:
		Entity();

			/*!
				\param name the name
				\param s the specialite
				\param x the abscissa
				\param y the ordinate
				\param player a boolean
			*/
		void modify(std::string, int, int, int, bool);
		std::string getName();

			/*!
				\param spe the specialite
			*/
		int getNbComp(int);

			/*!
				\param nb our number
				\param s the string to parse
			*/
		bool ourNumberIsPresent(int, std::string);

			/*!
				\param PV the pv
			*/
		void setPv(int);

			/*!
				\param pvmax the pv mac
			*/
		void setPvMax(int);

			/*!
				\param manamax the mana max
			*/
		void setManaMax(int);
		int getPv();
		int getPvMax();

			/*!
				\param mana the mana
			*/
		void setMana(int);
		int getMana();
		int getManaMax();
		bool getPlayable();
		int getX();
		int getY();

			/*!
				\param x the abscissa
			*/
		void setX(int);

			/*!
				\param y the ordinate
			*/
		void setY(int);
		bool isAlive();
		void kill();
		void listComp();
		std::string listCompStr();
		void assignNewCompetences();

			/*!
				\param string to parse
			*/
		int getPvOrManaFromStr(std::string);
		int getSpeInt();
		std::string getSpeName();

			/*!
				\param i the index for competence array
			*/
		int getOurPv(int);
			/*!
				\param i the index for competence array
			*/
		int getOurMana(int);

			/*!
				\param i the index for competence array
			*/
		int getTheirPv(int);

			/*!
				\param i the index for competence array
			*/
		int getTheirMana(int);

			/*!
				\param i the index for competence array
			*/
		std::string getCompName(int);

			/*!
				\param txt the text to analyze
			*/
		bool verifInt(std::string);
};

#endif