#include <iostream>
#include "Output.h"

using namespace std;


/// Constructor.
Output::Output(){
	content = "ready";
}


/// The "big switch". A-l-l the inputs.
void Output::display(string id, string param){
	if(id == "ChoosePlayerName")
		cout << "Choisissez le nom de votre joueur." << endl;
	else if(id == "ChoosePlayerSpe")
		cout << "Choisissez maintenant sa spécialité parmi celles disponibles:\n" << param << endl;
	else if(id =="ChooseInputType")
		cout << "Pour finir, choisissez le type d'input que vous souhaitez utiliser.\n>  \"t\" = touches du clavier\n>  n'importe quoi d'autre = commandes à entrer" << endl;
	else if(id == "InitializationComplete")
		cout << "Le monde a été créé avec succès. Vous êtes arrivé." << endl;
	else if(id == "ChooseAction")
		cout << "Que voulez-vous faire ?" << endl;
	else if(id == "ActionUp")
		cout << "Vous vous déplacez vers le nord." << endl;
	else if(id == "ActionUpError")
		cout << "Vous ne pouvez pas vous déplacer vers le nord." << endl;
	else if(id == "ActionDown")
		cout << "Vous vous déplacez vers le sud." << endl;
	else if(id == "ActionDownError")
		cout << "Vous ne pouvez pas vous déplacer vers le sud." << endl;
	else if(id == "ActionLeft")
		cout << "Vous vous déplacez vers l'ouest." << endl;
	else if(id == "ActionLeftError")
		cout << "Vous ne pouvez pas vous déplacer vers l'ouest." << endl;
	else if(id == "ActionRight")
		cout << "Vous vous déplacez vers l'est." << endl;
	else if(id == "ActionRightError")
		cout << "Vous ne pouvez pas vous déplacer vers l'est." << endl;
	else if(id =="UnauthorizedAction")
		cout << "Aucune action n'est attribuée à cette commande. Recommencez." << endl; 
	else if(id =="KillMonster")
		cout << "Vous avez tué un monstre." << endl;
	else if(id =="KillMonster")
		cout << "Vous avez tué un monstre." << endl;
	else if(id =="ActionLook")
		cout << "Vous regardez autour de vous. " << param << endl;
	else if(id =="PlayerInfos")
		cout << param << endl;
	else if(id == "ListCompetences")
		cout << param << endl;
	else if(id == "WowThatsAMonster")
		cout << param << endl;
	else if(id == "Attack")
		cout << "Vous attaquez le monstre avec [" << param << "]." << endl;
	else if(id == "Attacked")
		cout << "Le monstre vous attaque avec [" << param << "]." << endl;
	else if(id == "MonsterDead")
		cout << "Vous avez tué " << param << " !" << endl << endl;
	else if(id == "NotEnoughMana")
		cout << "Vous n'avez pas assez de mana." << endl;
	else if(id == "EnoughMana")
		cout << "Vous ne pouvez pas récupérer plus de mana." << endl;
	else if(id == "Obstacle")
		cout << "Vous ne pouvez pas aller là." << endl;
	else if(id == "PursueMonster")
		cout << "Votre radar indique qu'un monstre se trouve près de " << param << "." << endl;
	else if(id =="ActionHelp")
		cout << "Aide:\n--Saisie clavier: \"left\",\"right\",\"up\",\"down\",\"look\",\"comp\",\"pursue\",\"help\"...\n--Appui sur les touches: \"z\",\"q\",\"s\",\"d\",\"l\",\"c\",\"p\",\"h\"..." << endl;
	else
		cout << "unknown error in Ouput.cpp file" << endl;
}