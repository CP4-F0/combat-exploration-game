#ifndef INPUT_H
#define INPUT_H

	class Input{

		bool inputType = false;
		std::string str;
		int i;
		//double db;
		//char c;

		public:
			/*!
				\param string the string to read
			*/
			void readTxt(std::string);
			std::string returnStr();
			int returnInt();
			
			/*!
				\param boolean the type to change for
			*/
			void changeInputType(bool);
			char getch();
			bool getInputType();
	};

#endif
