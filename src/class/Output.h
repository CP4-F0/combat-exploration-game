#ifndef OUTPUT_H
#define OUTPUT_H

	class Output{

		std::string content;

		public:
			Output();

			/*!
				\param string the "case" for the handmade switch
				\param string additionnal informations from all over the game
			*/
			void display(std::string, std::string);
	};

#endif
