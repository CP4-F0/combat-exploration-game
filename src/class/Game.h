#ifndef GAME_H
#define GAME_H
#include "Entity.h"
#include "Input.h"
#include "Output.h"
#include "Map.h"

	class Game{
		int etat;
		Entity player;
		Specialite spe;
		Map world;
		Input in;
		Output out;

		public:
			Game();
			void debugDisplayPers();
			std::string debugInfosPers();
			void play();

			/*!
				\param action the action 
			*/
			void action(std::string);

			/*!
				\param action the action
			*/
			bool authorizedAction(std::string);
			void victory();
			void defeat();

			/*!
				\param string the content we have to display
				\param size the length of the string
				\param delay the duraction beetween each output 
			*/
			void writeTextLetterByLetter(std::string, int, int);
			

			/*!
				\param int for the modulo   
			*/
			int getRandomNumber(int);
			std::string listAllSpeNames();
			void clrScr();
	};

#endif
