#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

#include "Specialite.h"

using namespace std;


/// Constructor of speciality.
Specialite::Specialite()
{
	spe = 1;
	pv = 12;
	mana = 7;
	name = "Guerrier bourrin";
	naming = true;
}

/// Overloaded constructor of speciality.
Specialite::Specialite(int specialite)
{
	spe = specialite;
	ifstream fichier("src/bdd/Specialite.txt", ios::in);
	if(fichier)
	{
		int spe, pv,mana;
		string contenu;
		while(getline(fichier, contenu))
		{
			fichier >> spe >> pv >> mana;
			if(spe==specialite)
			{
				this->pv=pv;
				this->mana=mana;
			}
		}
		fichier.close();
	}
	else
		cerr << "Impossible d'ouvrir le fichier !" << endl;
}


/// A "sort of" constructor of speciality.
void Specialite::modify(int specialite)
{
	spe = specialite;
	ifstream fichier("src/bdd/Specialite.txt", ios::in);
	if(fichier)
	{
		bool iChooseThisOne = false;
		string contenu, name;
		while(getline(fichier, contenu)){
			getline(fichier, contenu, '|');
			if(stoi(contenu) == specialite){
				iChooseThisOne = true;
				this->spe = specialite;
			}
			getline(fichier, contenu, '|');
			if(iChooseThisOne)
				this->pv = stoi(contenu);
			getline(fichier, contenu, '|');
			if(iChooseThisOne)
				this->mana = stoi(contenu);
			getline(fichier, contenu, '|');
			if(iChooseThisOne){
				this->name = contenu;
			}
			getline(fichier, contenu, '|');
			if(iChooseThisOne){
				if(contenu == "1")
					this->naming = true;
				else
					this->naming = false;
				iChooseThisOne = false;
			}
		}
		fichier.close();
	}
	else
		cerr << "Impossible d'ouvrir le fichier !" << endl;
}


/// Getter.
int Specialite::getPv(){
	return pv;
}

/// Getter.
int Specialite::getMana(){
	return mana;
}

/// Getter.
int Specialite::getSpe(){
	return spe;
}

/// Getter.
string Specialite::getName(){
	return name;
}