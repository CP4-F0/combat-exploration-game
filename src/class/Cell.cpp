#include <iostream>
#include "Cell.h"

using namespace std;

/// Constructor.
Cell::Cell(){ // basic map
	visitable = true;
	appearence = ' ';
	color = "\e[42m";
	description = "C'est de l'herbe.";
	special = 0;
}

/// Overloaded constructor.
Cell::Cell(int i){
	if(i == 0){
		visitable = true;
		appearence = ' ';
		color ="\e[42m";
		description = "C'est de l'herbe.";
		special = 0;
	}
	else if(i == 1){
		visitable = true;
		appearence = ' ';
		color = "\e[100m";
		description = "Il n'y a que de la roche.";
		special = 0;
	}
	else if(i == 2){
		visitable = false;
		appearence = ' ';
		color = "\e[48;5;139m";
		description = "La montagne est impraticable.";
		special = 0;
	}
	else if(i == 3){
		visitable = false;
		appearence = ' ';
		color = "\e[48;5;22m";
		description = "C'est un arbre.";
		special = 0;
	}
	else if(i == 4){
		visitable = false;
		appearence = ' ';
		color = "\e[104m";
		description = "C'est une rivière.";
		special = 0;
	}
	else if(i == 5){
		visitable = true;
		appearence = ' ';
		color ="\e[42m";
		description = "C'est de l'herbe. Quelque chose semble biller entre deux brins d'herbe...";
		special = 1;
	}
	else if(i == 6){
		visitable = true;
		appearence = ' ';
		color = "\e[48;5;52m";
		description = "C'est une arbre, un reflet dans une branche arrive à capter votre regard...";
		special = 1;
	}
}

/// Getter.
bool Cell::getCellStatus(){
	return visitable;
}

/// Getter.
char Cell::getCellAppearence(){
	return appearence;
}

/// Getter.
std::string Cell::getCellDescription(){
	return description;
}

/// Getter.
int Cell::getCellSpecial(){
	return special;
}

/// Getter.
std::string Cell::getCellBackgroundColor(){
	return color;
}