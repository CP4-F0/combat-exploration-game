#ifndef COMPETENCE_H
#define COMPETENCE_H

class Competence
{
	private:
		int OurPv;
		int OurMana;
		int TheirPv;
		int TheirMana;
		std::string description;

	public:
		Competence();

		/*!
			\param ourPv our PV change
			\param ourMana our Mana change
			\param theirPv their PV change
			\param theirMana their Mana change
			\param descr the description of the competence
		*/
		Competence(int, int, int, int, std::string);
		void display();
		int getOurPv();
		int getOurMana();
		int getTheirPv();
		int getTheirMana();
		std::string getDesc();
		std::string getInfos();
		//~Competence();
};
#endif