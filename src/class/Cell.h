#ifndef CELL_H
#define CELL_H

	class Cell{
		bool visitable;
		char appearence;
		std::string color; // thx http://misc.flogisoft.com/bash/tip_colors_and_formatting
		std::string description;
		int special;

		public:
			
			Cell();
			/*!
				\param i the type of cell
			*/
			Cell(int);
			bool getCellStatus();
			char getCellAppearence();
			std::string getCellDescription();
			int getCellSpecial();
			std::string getCellBackgroundColor();

	};

#endif
