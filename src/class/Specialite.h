#ifndef SPECIALITE_H
#define SPECIALITE_H

class Specialite
{
	private:
		int spe;
		int pv;
		int mana;
		std::string name;
		bool naming; // mana or energy ?

	public:
		Specialite();
		
		/*!
			\param int the speciality to load
		*/
		Specialite(int);

		/*!
			\param int the speciality to change
		*/
		void modify(int);
		int getPv();
		int getMana();
		int getSpe();
		std::string getName();
};

#endif