#include <iostream>
#include <unistd.h>
#include <termios.h>
#include "Input.h"

using namespace std;

/// Getter (directly from cin or buffer).
void Input::readTxt(string type){
	if(!inputType){
		cout << ">>";
		if(type == "String")
			cin >> str;
		else if(type == "Int")
			cin >> i;
		else
			cout << "Unknown error in Input.cpp file" << endl;
	}
	else{
		if(type == "String")
			str = getch();
		else
			cin >> i;
	}
}

/// Getter.
std::string Input::returnStr(){
	return str;
}

/// Getter.
int Input::returnInt(){
	return i;
}

/// Setter.
void Input::changeInputType(bool inputType){
	this->inputType = inputType;
}

/// Getter.
bool Input::getInputType(){
	return inputType;
}

/// Getter-from-buffer function. Thanks http://stackoverflow.com/a/912796
char Input::getch(){
	char buf = 0;
	struct termios old = {0};
	if(tcgetattr(0, &old) < 0)
		perror("tcsetattr()");
	old.c_lflag &= ~ICANON;
	old.c_lflag &= ~ECHO;
	old.c_cc[VMIN] = 1;
	old.c_cc[VTIME] = 0;
	if(tcsetattr(0, TCSANOW, &old) < 0)
		perror("tcsetattr ICANON");
	if(read(0, &buf, 1) < 0)
		perror ("read()");
	old.c_lflag |= ICANON;
	old.c_lflag |= ECHO;
	if(tcsetattr(0, TCSADRAIN, &old) < 0)
		perror ("tcsetattr ~ICANON");
	return (buf);
}