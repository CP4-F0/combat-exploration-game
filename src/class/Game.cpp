#include <iostream>
#include <unistd.h>
#include <ctime>
#include <fstream>
#include "Map.h"
#include "Game.h"

using namespace std;



/// Constructor. Initialize the game.
	Game::Game(){

		// INTRO - début jeu
		writeTextLetterByLetter("Welcome in the vast kingdom of RANDOMIA o/", 42, 60000);
		writeTextLetterByLetter("\n\n\n\n\n", 5, 80000);

		etat = 1; // view map, explore the vast lands of RANDOMIA \o/
		out.display("ChoosePlayerName", "");

		in.readTxt("String");
		string name = in.returnStr();

		out.display("ChoosePlayerSpe", listAllSpeNames());
		
		in.readTxt("Int");
		int spe = in.returnInt();
		
		player.modify(name, spe, 1, 1, true);

		out.display("ChooseInputType", "");

		in.readTxt("String");

		if(in.returnStr() == "t") // super keyboard mode !
			in.changeInputType(true);

	}


/// Debug feature.
	void Game::debugDisplayPers(){
		cout << "[" << player.getName() << "] - PV:" << player.getPv() << " - MANA:" << player.getMana() << endl;
	}


/// Home of th main loop.
	void Game::play(){ // AND THAT, MY FRIENDS, IS WHEN THE MAGICAL SIMULATION STARTED

		clrScr();

		out.display("InitializationComplete", "");

		world.displayInterface(player.getX(), player.getY());

		while(player.getPv() > 0 && world.getNbMonsters() > 0){


			if(etat == 1 && world.isThereAMonsterRightHere(player.getX(),player.getY()) != -1){
				etat = 2; // FIGHT !
			}

			if(etat == 1){ // adventure/exploration

				out.display("ChooseAction", "");

				clrScr();

				do{
					in.readTxt("String");
					if(!authorizedAction(in.returnStr()))
						out.display("UnauthorizedAction", "");
				} while(!authorizedAction(in.returnStr()));
				action(in.returnStr());

				world.displayInterface(player.getX(), player.getY());

				out.display("PlayerInfos",debugInfosPers());

			}
			else if(etat == 2){ // fight

				out.display("PlayerInfos",debugInfosPers());
				out.display("WowThatsAMonster", world.displayInfosMonster(world.isThereAMonsterRightHere(player.getX(),player.getY())));
				world.displayInterface(player.getX(), player.getY());

				out.display("ChooseAction", "");

				out.display("ListCompetences", player.listCompStr());
				in.readTxt("Int");

				clrScr();
				action("attack");

				//world.killMonster(player.getX(), player.getY());
				//out.display("KillMonster", "");
				etat = 1;
			}
		}
		if(player.getPv() <= 0){
			defeat();
		}
		else{
			victory();
		}
	}


/// When you want to do something.
	void Game::action(string action){
		if(etat == 1){ // balade sur la map
			// récupération PV & Mana
			if(player.getMana() < player.getManaMax()){
				player.setMana(player.getMana()+1);
			}

			if(getRandomNumber(5) == 2 && player.getPv() < player.getPvMax())
				player.setPv(player.getPv()+1);

			if(
				(
					!in.getInputType() && action == "up"
				)
				||
				(
					in.getInputType() && action == "z"
				)
			){
				if(player.getX()>0){
					if(world.canWeGoHere(player.getX()-1,player.getY())){
						player.setX(player.getX()-1);
						out.display("ActionUp", "");
					}
					else
						out.display("Obstacle", "");
				}
				else
					out.display("ActionUpError", "");
			}
			else if(
				(
					!in.getInputType() && action == "down"
				)
				||
				(
					in.getInputType() && action == "s"
				)
			){
				if(player.getX()<world.getSizeMap()-1){
					if(world.canWeGoHere(player.getX()+1,player.getY())){
						player.setX(player.getX()+1);
						out.display("ActionDown", "");
					}
					else
						out.display("Obstacle", "");
				}
				else
					out.display("ActionDownError", "");
			}
			else if(
				(
					!in.getInputType() && action == "left"
				)
				||
				(
					in.getInputType() && action == "q"
				)
			){
				if(player.getY()>0){
					if(world.canWeGoHere(player.getX(),player.getY()-1)){
						player.setY(player.getY()-1);
						out.display("ActionLeft", "");
					}
					else
						out.display("Obstacle", "");
				}
				else
					out.display("ActionLeftError", "");
			}
			else if(
				(
					!in.getInputType() && action == "right"
				)
				||
				(
					in.getInputType() && action == "d"
				)
			){
				if(player.getY()<world.getSizeMap()-1){
					if(world.canWeGoHere(player.getX(),player.getY()+1)){
						player.setY(player.getY()+1);
						out.display("ActionRight", "");
					}
					else
						out.display("Obstacle", "");
				}
				else
					out.display("ActionRightError", "");
			}
			else if(
				(
					!in.getInputType() && action == "look"
				)
				||
				(
					in.getInputType() && action == "l"
				)
			){
				out.display("ActionLook", world.getAppearence(player.getX(), player.getY()));
			}
			else if(
				(
					!in.getInputType() && action == "help"
				)
				||
				(
					in.getInputType() && action == "h"
				)
			){
				out.display("ActionHelp", "");
			}
			else if(
				(
					!in.getInputType() && action == "comp"
				)
				||
				(
					in.getInputType() && action == "c"
				)
			){
				out.display("ListCompetences", player.listCompStr());
			}
			else if(
				(
					!in.getInputType() && action == "pursue"
				)
				||
				(
					in.getInputType() && action == "p"
				)
			){
				out.display("PursueMonster", world.getIndicationMonster());
			}
			else{
				cout << "Unknown action." << endl;
			}
		}
		else if(etat == 2){ // FIGHT !
			if(action == "attack"){

				int comp = in.returnInt()-1;

				int idMonster = world.isThereAMonsterRightHere(player.getX(),player.getY());

				if(player.getMana() >= 0-player.getOurMana(comp)){

					out.display("Attack", player.getCompName(comp));

					world.attack(player.getTheirPv(comp), player.getTheirMana(comp), idMonster);

					if(world.isMonsterDead(idMonster)){
						out.display("MonsterDead", world.getMonsterName(idMonster));
						world.displayInterface(player.getX(), player.getY());


						// sorte de level up (progression linéaire = caca)
						player.setPvMax(player.getPvMax()+2);
						player.setManaMax(player.getManaMax()+2);

						out.display("PlayerInfos",debugInfosPers());
					}

					player.setPv(player.getPv()+player.getOurPv(comp));
					// encore un exemple de test unitaire
					//cout << player.getMana()+player.getOurMana(comp) << "/" << player.getManaMax() << endl;
					if(player.getMana()+player.getOurMana(comp) <= player.getManaMax())
						player.setMana(player.getMana()+player.getOurMana(comp));
					else
						out.display("EnoughMana", "");

				}
				else{
					out.display("NotEnoughMana", "");
				}

				// au monstre d'attaquer, s'il est toujours vivant
				if(!world.isMonsterDead(idMonster)){
					int comp;
					Entity monster = world.getMonster(idMonster);
					
					do{
						comp = getRandomNumber(world.getNbComp(idMonster));
					}while(monster.getMana() < 0-monster.getOurMana(comp));


					// on a forcément une bonne attaque là
					out.display("Attacked", monster.getCompName(comp));
					player.setPv(player.getPv()+monster.getTheirPv(comp));
					player.setMana(player.getMana()+monster.getTheirMana(comp));

				}
			}
		}
	}


/// Service-verification of actions.
	bool Game::authorizedAction(string action){
		string valid[17] = {"up","left","down","right","look","comp","pursue","z","q","s","d","l","h","c","p"};
		for(int i(0); i < 17; i++){
			if(action == valid[i])
				return true;
		}
		return false;
	}


/// A non-so-debug-feature that is shown at each turn.
	string Game::debugInfosPers(){
		return player.getName() +"("+ player.getSpeName() +") ["+ to_string(player.getX()) +";"+ to_string(player.getY()) +"] PV["+ to_string(player.getPv()) +"] MANA[" + to_string(player.getMana()) +"] - MAP[" + to_string(world.getSizeMap()) +"x"+ to_string(world.getSizeMap()) +"] - ["+ to_string(world.getNbMonsters()) +"] monstres.";
	}


/// Victory animation. And "champagne"!
	void Game::victory(){

		writeTextLetterByLetter("\n\n\n\n\n", 5 , 400000);

		// thx http://patorjk.com/software/taag/
		cout << ":::     ::: ::::::::::: :::::::: ::::::::::: ::::::::  :::::::::  :::   :::" << endl;
		usleep(400000);
		cout << ":+:     :+:     :+:    :+:    :+:    :+:    :+:    :+: :+:    :+: :+:   :+:" << endl;
		usleep(400000);
		cout << "+:+     +:+     +:+    +:+           +:+    +:+    +:+ +:+    +:+  +:+ +:+" << endl;
		usleep(400000);
		cout << "+#+     +:+     +#+    +#+           +#+    +#+    +:+ +#++:++#:    +#++:" << endl;
		usleep(400000);
		cout << " +#+   +#+      +#+    +#+           +#+    +#+    +#+ +#+    +#+    +#+" << endl;
		usleep(400000);
		cout << "  #+#+#+#       #+#    #+#    #+#    #+#    #+#    #+# #+#    #+#    #+#" << endl;
		usleep(400000);
		cout << "    ###     ########### ########     ###     ########  ###    ###    ###" << endl;
		usleep(400000);
		writeTextLetterByLetter("\n\n\n\n\n\n\n\n", 8 , 400000);

		writeTextLetterByLetter("Thanks for playing,", 19, 60000);
		cout << endl;
		writeTextLetterByLetter("    That's all folks !", 22, 60000);
	}


/// Defeat animation.
void Game::defeat(){

	writeTextLetterByLetter("\n\n\n\n", 4 , 400000);
	
	cout << "    ▄▄ •  ▄▄▄· • ▌ ▄ ·. ▄▄▄ .           ▌ ▐·▄▄▄ .▄▄▄  " << endl;
	usleep(400000);
	cout << "   ▐█ ▀ ▪▐█ ▀█ ·██ ▐███▪▀▄.▀·    ▪     ▪█·█▌▀▄.▀·▀▄ █·" << endl;
	usleep(400000);
	cout << "   ▄█ ▀█▄▄█▀▀█ ▐█ ▌▐▌▐█·▐▀▀▪▄     ▄█▀▄ ▐█▐█•▐▀▀▪▄▐▀▀▄ " << endl;
	usleep(400000);
	cout << "   ▐█▄▪▐█▐█ ▪▐▌██ ██▌▐█▌▐█▄▄▌    ▐█▌.▐▌ ███ ▐█▄▄▌▐█•█▌" << endl;
	usleep(400000);
	cout << "   ·▀▀▀▀  ▀  ▀ ▀▀  █▪▀▀▀ ▀▀▀      ▀█▄▀▪. ▀   ▀▀▀ .▀  ▀" << endl;
		usleep(400000);
		writeTextLetterByLetter("\n\n\n\n\n\n\n\n", 8 , 400000);

		writeTextLetterByLetter("Thanks for playing,", 19, 60000);
		cout << endl;
		writeTextLetterByLetter("    That's all folks !", 22, 60000);
}


/// A design-only not so important function.
void Game::writeTextLetterByLetter(string s, int size, int delay){
	for(int i(0); i<size; i++){
		cout << s[i];
		fflush(stdout); // thanks http://stackoverflow.com/a/2259329
		usleep(delay);
	}
}


/// Sort of a getter.
string Game::listAllSpeNames(){
	string speNames;

	ifstream fichier("src/bdd/Specialite.txt", ios::in);
	if(fichier)
	{
		string contenu;
		for(int i(1); getline(fichier, contenu); i++){
			getline(fichier, contenu, '|');
			getline(fichier, contenu, '|');
			getline(fichier, contenu, '|');
			getline(fichier, contenu, '|');
			if(i>1)
				speNames += "| ";
			speNames += to_string(i) +" - "+ contenu +" ";
			getline(fichier, contenu, '|');
		}
		fichier.close();
	}
	else
		cerr << "Impossible d'ouvrir le fichier !" << endl;

	return speNames;
}


/// Clear the screen. Thanks http://stackoverflow.com/q/4062045
void Game::clrScr(){
		cout << "\033[2J\033[1;1H"; // thx http://stackoverflow.com/q/4062045
}

/// Add a little of randomness in this world.
int Game::getRandomNumber(int max){
	return (rand()%max);
}