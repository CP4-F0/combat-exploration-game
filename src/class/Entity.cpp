#include <iostream>
#include <fstream>
#include <string>

#include "Entity.h"

using namespace std;

/// Constructor
Entity::Entity()
{
	name="NULL";
	pv=10;
	pvMax=10;
	mana=10;
	manaMax=10;
	player=true;
	dead = false;
	nbComp=0;
}

/// Modify function.
void Entity::modify(string name, int s, int x, int y, bool player)
{
	this->name = name;
	
	// spe
	spe.modify(s);
	this->pv = spe.getPv();
	this->pvMax = spe.getPv();
	this->mana = spe.getMana();
	this->manaMax = spe.getMana();

	// comp
	this->nbComp = getNbComp(spe.getSpe());
	if(getNbComp(spe.getSpe())==0)
	{
		this->nbComp=1;
		Competence N(0,-4,-5,0,"Coup de poire");
		this->c=new Competence[1];
		this->c[0]= N;
		cerr << "Specialité sans competénce, création d'une compétence par défaut."<<endl;	
	}
	else
	{
		this->c = new Competence[nbComp];
		assignNewCompetences();
	}
	// practical informations
	this->player = player;
	this->x = x;
	this->y = y;
}

/// Getter (from a txt file).
int Entity::getNbComp(int spe)
{
	string contenu;
	int nbComp = 0;
	ifstream fichier("src/bdd/Competence.txt", ios::in);
	if(fichier)
	{
		for(int i(0); getline(fichier, contenu, '|'); i++){
			if(i>7){
				if(i%7 == 1){
					if(ourNumberIsPresent(spe, contenu))
						nbComp++;
				}
			}
		}
		fichier.close();
	}
	else
		cerr << "Impossible d'ouvrir le fichier !" << endl;

	return nbComp;
}

/// A parser.
bool Entity::ourNumberIsPresent(int nb, string s){
	int n = 0;
	for(int i(s.length()); i>=0; i--){
		if(i<s.length() && s[i] != ',' && i>0 && s[i-1] != ',')
			n = (s[i-1]-48)*10;

		if(s[i]-48+n == nb)
			return true;

		if(i<s.length() && s[i] != ',' && i > 0 && s[i-1] != ',')
			i--;
	}
	return false;
}


/// Getter.
std::string Entity::getName(){
	return name;
}

/// Getter.
int Entity::getPv(){
	return pv;
}

/// Getter.
int Entity::getMana(){
	return mana;
}

/// Getter.
int Entity::getPvMax(){
	return pvMax;
}

/// Getter.
int Entity::getManaMax(){
	return manaMax;
}

/// Getter.
bool Entity::getPlayable(){
	return player;
}

/// Getter.
int Entity::getX(){
	return x;
}

/// Getter.
int Entity::getY(){
	return y;
}

/// Setter.
void Entity::setX(int x){
	this->x = x;
}

/// Setter.
void Entity::setY(int y){
	this->y = y;
}

/// Getter.
bool Entity::isAlive(){
	return !dead;
}

/// Setter.
void Entity::kill(){
	dead = true;
}

/// Sort of visual getter.
void Entity::listComp(){
	for(int i(0); i<nbComp; i++){
		c[i].display();
	}
}

/// Getter.
string Entity::listCompStr(){
	string comp = "";
	for(int i(0); i<nbComp; i++){
		comp += "\n"+ to_string(i+1) + ")  " + c[i].getInfos();
	}
	return comp;
}

/// Constrol function.
bool Entity::verifInt(string contenu)
{
	for(int i=1;i<contenu.size();i++)
	{
		if(!isdigit(contenu[i]))
			return false;
	}
	return true;

}

/// Big and bloated setter.
void Entity::assignNewCompetences(){
	ifstream fichier("src/bdd/Competence.txt", ios::in);
	string contenu, desc, spe;
	int ourPv, ourMana, theirPv, theirMana, nbComp = 0;
	if(fichier)
	{
		for(int i(0); getline(fichier, contenu, '|'); i++){
			if(i>=7){
				
				if(contenu[0] == '\n'){
					contenu = contenu.erase(0, 1);
					// if we have to get the id, its here !
				}

				getline(fichier, contenu, '|');
				spe = contenu;

				getline(fichier, contenu, '|');
				ourPv = getPvOrManaFromStr(contenu);

				getline(fichier, contenu, '|');
				ourMana = getPvOrManaFromStr(contenu);

				getline(fichier, contenu, '|');
				theirPv = getPvOrManaFromStr(contenu);

				getline(fichier, contenu, '|');
				theirMana = getPvOrManaFromStr(contenu);

				getline(fichier, contenu, '|');
				desc = contenu;

				// ligne suivante => vestige de test unitaire de la fonction qui fait office de parser de fichiers
				//cout << "-> ["<< id <<"/"<< spe <<"/"<< ourPv <<"/"<< ourMana << "/" << theirPv << "/" << theirMana <<"/"<< desc <<"]" << endl;

				if(ourNumberIsPresent(this->spe.getSpe(), spe)){
					Competence A(ourPv, ourMana, theirPv, theirMana, desc);
					c[nbComp] = A;
					nbComp++;
				}
			}
		}
		fichier.close();
	}
	else{
		cerr << "[error:Entity.cpp:16X] Impossible d'ouvrir le fichier !" << endl;
	}
}

/// Little parser.
int Entity::getPvOrManaFromStr(string pvOrMana)
{
	if(!verifInt(pvOrMana))
		return 0;
	else
	{
		if(pvOrMana[0] == '-')
		{
			pvOrMana = pvOrMana.erase(0, 1);
			return (0 - stoi(pvOrMana));
		}	
		else
		{
			pvOrMana = pvOrMana.erase(0, 1);
			return stoi(pvOrMana);
		}
	}
}

/// Getter.
string Entity::getSpeName(){
	return spe.getName();
}

/// Getter.
int Entity::getSpeInt(){
	return spe.getSpe();
}

/// Setter.
void Entity::setPv(int pv){
	this->pv = pv;
}

/// Setter.
void Entity::setMana(int mana){
	this->mana = mana;
}

/// Setter.
void Entity::setPvMax(int pv){
	this->pvMax = pv;
}

/// Setter.
void Entity::setManaMax(int mana){
	this->manaMax = mana;
}

/// Getter.
int Entity::getOurPv(int i){
	return c[i].getOurPv();
}

/// Getter.
int Entity::getOurMana(int i){
	return c[i].getOurMana();
}

/// Getter.
int Entity::getTheirPv(int i){
	return c[i].getTheirPv();
}

/// Getter.
int Entity::getTheirMana(int i){
	return c[i].getTheirMana();
}

/// Getter.
string Entity::getCompName(int i){
	return c[i].getDesc();
}
