#ifndef MAP_H
#define MAP_H
#include "Cell.h"
#include "Entity.h"

	class Map
	{
		int sizeMap;
		int sizeMonsters;
		Cell **cell;
		Entity *monster;
		std::string basicBackgroundColor = "\e[49m";
		std::string playerColor = "\e[37m";
		std::string basicTextColor = "\e[39m";
		std::string notACellBackground = "\e[48;5;237m";

		public:
			Map();
			~Map();
			/*!
				\param x the abscissa
				\param y the ordinate
			*/
			void displayInterface(int, int);

			void listMonsters();
			int getNbMonsters();
			
			/*!
				\param x the abscissa
				\param y the ordinate
			*/
			int isThereAMonsterRightHere(int, int);
			

			/*!
				\param x the abscissa
				\param y the ordinate
			*/
			bool killMonster(int, int);
			int getSizeMap();
			

			/*!
				\param x the abscissa
				\param y the ordinate
			*/
			std::string getAppearence(int, int);
			

			/*!
				\param i the maximum value for the modulo
			*/
			int getRandomNumber(int);


			/*!
				\param int pv effet
				\param int mana effet
				\param int i the index
			*/
			bool attack(int, int, int);

			/*!
				\param i the index
			*/
			std::string displayInfosMonster(int);

			/*!
				\param i the index
			*/
			bool isMonsterDead(int);

			/*!
				\param i the index
			*/
			std::string getMonsterName(int);
			
			/*!
				\param x the abscissa
				\param y the ordinate
			*/
			bool canWeGoHere(int, int);
			std::string getIndicationMonster();
			
			/*!
				\param spe the index of speciality
			*/
			int getNbComp(int);

			/*!
				\param i the index
			*/
			Entity getMonster(int);

	};

#endif
