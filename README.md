# combat-exploration-game

## Why ?
It's a school project.

## Who ?
We're 3; David, Kevin and Corentin.

## What ?
The game is actually programmed in C++.

## How do I compile ?
~~We know, our Makefile is broken. Use `clear;clear;g++ main.cpp src/class/*.cpp -o main -std=c++11` instead.~~  
Fixed! Thanks Booti386!

## WHATDOESITLOOKSLIKE???
Just like this :

### Videos :  
![Indev video 3](http://l3m.in/p/up/files/1494473827-combat-exploration-game-final.webm)  
[Indev video 2](http://l3m.in/p/up/files/1493745114-videos.php?v=1493744840-indev-ceg.webm)  
[Indev video 1](http://l3m.in/p/up/files/1493745114-videos.php?v=1493563634-indev-test.webm)

### Screenshots :  
Actual version :

![trees+mountain+river](https://i.snag.gy/fgOLmn.jpg "a mountain, a river and some trees")

![end of a fight](https://i.snag.gy/6cMuiF.jpg "the end of a fight")

Dev versions :

![indev colours](https://i.snag.gy/2Y3IdT.jpg "just added colours !")

![indev](https://i.snag.gy/tLSYjF.jpg "Isnt it amazing ? :)")

![indev](https://i.snag.gy/RPnGgh.jpg "Isnt it amazing ? :)")