#include <iostream>

#include "src/class/Game.h"

using namespace std;

// Thx to https://git.bitmycode.com/Booti386/makesys for our makefile

int main(){

	Game leJeu;

	leJeu.debugDisplayPers();

	leJeu.play();

	cout << endl;
	return 0;
}
