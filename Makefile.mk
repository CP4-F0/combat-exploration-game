main = main$(EXE_EXT)

BINS-CXX-y := $(main)

objs := \
    main.cpp.o \
    src/class/Cell.cpp.o \
    src/class/Competence.cpp.o \
    src/class/Entity.cpp.o \
    src/class/Game.cpp.o \
    src/class/Input.cpp.o \
    src/class/Map.cpp.o \
    src/class/Output.cpp.o \
    src/class/Specialite.cpp.o \

OBJS-$(main)-y := $(objs)